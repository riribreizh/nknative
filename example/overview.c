#include "../nknative.h"
#include "../nuklear/demo/common/overview.c"

int main(void) {
    nkn_create("Nuklear overview", 780, 640);
    while (nkn_events(-1)) {
        if (!overview(nkn_context())) break;
        nkn_render(nk_rgb(35,43,76));
    }
    nkn_destroy();
    return 0;
}
