OUTPUT = nknative.h
NK_FILES = nuklear/nuklear.h nuklear/example/stb_image.h nuklear/demo/x11_xft/nuklear_xlib.h

.PHONY: all clean example

all: $(OUTPUT) example

clean:
	-rm -f $(OUTPUT)
	$(MAKE) -C example clean

$(OUTPUT): Makefile src/build.sh src/nkn_pre.h src/nkn.h src/nkn_x11.c $(NK_FILES)
	cd src && sh build.sh > ../$(OUTPUT)

example:
	$(MAKE) -C $@
