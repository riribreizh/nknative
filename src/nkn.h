#ifndef NK_NATIVE_H_
#define NK_NATIVE_H_

#include "nkn_pre.h"

int nkn_create(const char *title, int w, int h);
void nkn_destroy(void);
struct nk_context *nkn_context(void);
struct nk_rect nkn_bounds(void);
int nkn_events(int wait);
void nkn_render(struct nk_color bg);

#ifdef NK_IMPLEMENTATION
#ifdef __linux__
#define NK_XLIB_IMPLEMENTATION
#ifndef NK_XLIB_USE_XFT
#define NK_XLIB_USE_XFT
#endif
#ifndef NK_XLIB_INCLUDE_STB_IMAGE
#define NK_XLIB_INCLUDE_STB_IMAGE
#endif
#define STB_IMAGE_IMPLEMENTATION

#endif /* _linux__ */
#endif /* NK_IMPLEMENTATION */


#endif /* !NK_NATIVE_H_ */
