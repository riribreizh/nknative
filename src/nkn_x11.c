#include "nkn.h"

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#include "../nuklear/demo/x11_xft/nuklear_xlib.h"
#ifdef NK_IMPLEMENTATION

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

static struct {
    Display *dpy;
    int screen;
    Colormap cmap;
    Window wnd;

    Atom wm_delete_window;

    int wait;
    int lastevent;
    struct nk_context *ctx;
} nkwnd;

static XFont *font;

static int timestamp(void) {
    struct timeval tv;
    if (gettimeofday(&tv, 0) < 0) return 0;
    return (int)tv.tv_sec * 1000 + (int)tv.tv_usec / 1000;
}

static void sleep_for(int t) {
    struct timespec ts;
    const time_t sec = (long)(t / 1000);
    const int ms = t - (int)sec * 1000;
    ts.tv_sec = sec;
    ts.tv_nsec = (long)ms * 1000000L;
    while (-1 == nanosleep(&ts, &ts));
}

int nkn_create(const char *title, int w, int h) {
    Window root;
    Visual *vis;
    XSetWindowAttributes xswa;
    XWindowAttributes xwa;

    memset(&nkwnd, 0, sizeof(nkwnd));
    nkwnd.dpy = XOpenDisplay(NULL);
    if (!nkwnd.dpy) return 0;
    nkwnd.screen = XDefaultScreen(nkwnd.dpy);

    root = RootWindow(nkwnd.dpy, nkwnd.screen);
    vis = DefaultVisual(nkwnd.dpy, nkwnd.screen);
    nkwnd.cmap = XCreateColormap(nkwnd.dpy, root, vis, AllocNone);
    xswa.colormap = nkwnd.cmap;
    xswa.event_mask = 0
        | ExposureMask | StructureNotifyMask
        | KeymapStateMask | KeyPressMask | KeyReleaseMask
        | ButtonPressMask | ButtonReleaseMask | ButtonMotionMask
        | PointerMotionMask
        ;
    nkwnd.wnd = XCreateWindow(nkwnd.dpy, root, 0, 0, w, h, 0,
        XDefaultDepth(nkwnd.dpy, nkwnd.screen), InputOutput,
        vis, CWColormap|CWEventMask, &xswa);
    XStoreName(nkwnd.dpy, nkwnd.wnd, title);
    XMapWindow(nkwnd.dpy, nkwnd.wnd);

    nkwnd.wm_delete_window = XInternAtom(nkwnd.dpy, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(nkwnd.dpy, nkwnd.wnd, &nkwnd.wm_delete_window, 1);
    XGetWindowAttributes(nkwnd.dpy, nkwnd.wnd, &xwa);

    font = nk_xfont_create(nkwnd.dpy, "Arial");

    nkwnd.ctx = nk_xlib_init(font, nkwnd.dpy, nkwnd.screen, nkwnd.wnd,
        vis, nkwnd.cmap, xwa.width, xwa.height);

    return 1;
}

void nkn_destroy(void) {
    nk_xfont_del(nkwnd.dpy, font);
    nk_xlib_shutdown();
    XUnmapWindow(nkwnd.dpy, nkwnd.wnd);
    XFreeColormap(nkwnd.dpy, nkwnd.cmap);
    XDestroyWindow(nkwnd.dpy, nkwnd.wnd);
    XCloseDisplay(nkwnd.dpy);
}

struct nk_context *nkn_context(void) {
    return nkwnd.ctx;
}

struct nk_rect nkn_bounds(void) {
    XWindowAttributes xwa;
    XGetWindowAttributes(nkwnd.dpy, nkwnd.wnd, &xwa);
    return nk_rect(xwa.x, xwa.y, xwa.width, xwa.height);
}

int nkn_events(int wait) {
    int ret = 1;
    XEvent event;

    nk_input_begin(nkwnd.ctx);
    while (XPending(nkwnd.dpy)) {
        XNextEvent(nkwnd.dpy, &event);
        if (event.type == ClientMessage) {
            ret = 0;
            break;
        }
        if (XFilterEvent(&event, nkwnd.wnd)) continue;
        nk_xlib_handle_event(nkwnd.dpy, nkwnd.screen, nkwnd.wnd, &event);
    }
    nk_input_end(nkwnd.ctx);

    nkwnd.wait = wait;
    nkwnd.lastevent = timestamp();

    return ret;
}

void nkn_render(struct nk_color bg) {
    int ts;

    XClearWindow(nkwnd.dpy, nkwnd.wnd);
    nk_xlib_render(nkwnd.wnd, bg);
    XFlush(nkwnd.dpy);

    ts = timestamp() - nkwnd.lastevent;
    if (ts < nkwnd.wait) sleep_for(nkwnd.wait - ts);
}

#endif /* NK_IMPLEMENTATION */
