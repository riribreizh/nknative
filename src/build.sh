#!/bin/sh

omit_includes () {
    cat "$1" | sed 's@#include ".*"$@@g'
}

omit_cpp_comments () {
    cat "$1" | sed 's@//.*$@@g'
}

omit_includes nkn_pre.h
cat ../nuklear/nuklear.h
omit_includes nkn.h
omit_cpp_comments ../nuklear/example/stb_image.h
# linux/xlib
echo -e '\n#ifdef __linux__\n'
omit_includes ../nuklear/demo/x11_xft/nuklear_xlib.h
omit_includes nkn_x11.c
echo -e '\n#endif /* __linux__ */'
# windows/gdi
