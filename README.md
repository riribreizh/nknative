# Nuklear native

One file Nuklear plus native backends (X11, Windows, Mac?) to quickly create
example/test programs with Nuklear without doing a lot of extra stuff around
the actual GUI definition.

It works the same than traditionnal Nuklear with `NK_IMPLEMENTATION` define
providing the implementation.

Depending on platforms, that will also include backend implementation plus
needed add-ons (like `stb_image`).

## Supported platforms

It's a work in progress, and only Linux/X11 has been tested for now.
Other platform will be tested someday, somewhere, in some universe.

| Platform | Implementation |
|----------|----------------|
| Linux    | X11/Xft with `demo/x11_xft/nuklear_xlib.h` |

## Update nknative.h

```sh
git clone .... nknative
cd nknative
git submodule init
# do your changes, then
make nknative.h
# (or make all to build the example)
```

## Nuklear macros automatically set

| Define | Platform | Description |
|--------|----------|-------------|
| `NK_INCLUDE_STANDARD_IO` | All | Use of `<stdio.h>` |
| `NK_INCLUDE_STANDARD_VARARGS` | All | Use of `<stdarg.h>` |
| `NK_INCLUDE_DEFAULT_ALLOCATOR` | All | Use of `<stdlib.h>` |
| `NK_XLIB_USE_XFT` | X11 | Use Freetype through Xft with Xlib |
| `NK_XLIB_INCLUDE_STB_IMAGE` | X11 | Insert STB image in header and use its functions |
| `NK_XLIB_IMPLEMENTATION` | X11 | If `NK_IMPLEMENTATION` is defined |
| `STB_IMAGE_IMPLEMENTATION` | X11 | If `NK_IMPLEMENTATION` is defined |
| `_POSIX_C_SOURCE` | X11 | Set to `200112L` |

## Window management API

Only 6 functions. See the example for how to use them:
```c
int nkn_create(const char *title, int w, int h);
```
Creates the window with given title and `w` width and `h` height.

```c
void nkn_destroy(void);
```
Destroys the window.

```c
struct nk_context *nkn_context(void);
```
Gives a pointer to the Nuklear context created with `nkn_create()`.

```c
struct nk_rect nkn_bounds(void);
```
Gives the bound rectangle of the window.

```c
int nkn_events(int wait);
```
Check platform events and populates Nuklear input. `wait` has 3 kind of values:
* `< 0`: waits forever until an event occurs
* `0`: don't wait
* `> 0`: wait for the specified time in seconds if no event occurs

```c
void nkn_render(struct nk_color bg);
```
Renders the window with native calls.
